﻿using System;
using System.Text;
using RabbitMQ.Client;

namespace TestHereAll
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Hello World!");
                var factory = new ConnectionFactory() { HostName = "104.199.5.197", UserName = "antoine", Password = "bMqC3g6n" };
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue: "Sample", durable: false, exclusive: false, autoDelete: false, arguments: null);

                        string message = "Hello World!";
                        var body = Encoding.UTF8.GetBytes(message);

                        channel.BasicPublish(exchange: "", routingKey: "hello", basicProperties: null, body: body);
                        Console.WriteLine(" [x] Sent {0}", message);
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}
