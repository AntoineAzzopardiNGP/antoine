'use strict'

const amqp = require('amqplib/callback_api');
let runningFunction = null;
let amqpConn = null;

class LocalRabbitMq {
    
    static async start(runningFunction) {

        var self = this;

        amqp.connect('amqp://localhost' + "?heartbeat=60", function (err, conn) {
            if (err) {
                console.error("[AMQP]", err.message);
                return setTimeout(self.start(runningFunction), 1000);
            }

            conn.on("error", function (err) {
                if (err.message !== "Connection closing") {
                    console.error("[AMQP] conn error", err.message);
                }
            });

            conn.on("close", function () {
                console.error("[AMQP] reconnecting");
                return setTimeout(self.start(runningFunction), 1000);
            });

            console.log("[AMQP] connected");
            amqpConn = conn;
            runningFunction;
        });
    }

};

module.exports = LocalRabbitMq;