var amqp = require('amqp-connection-manager');
var connection = amqp.connect(['amqp://localhost']);
connection.on('connect', function () {
    console.log('Connected!');
});


var exchange = "DeadQueueTest";
var type = "direct";
var queue = "DeadQueueTest";
var routePey = "R1";


var channelWrapper = connection.createChannel({
    name: "DeadLetter",
    json: false,
    setup: function (channel) {
        // `channel` here is a regular amqplib `ConfirmChannel`.
        // Note that `this` here is the channelWrapper instance.
        var args = {
            arguments: {'x-dead-letter-exchange': 'WorkExchange'}
        }
                     
        return Promise.all([
            channel.assertQueue("my-queue", { exclusive: false, autoDelete: false }),
            channel.consume("my-queue", handleMessage),
           
        ])
    }
});

var handleMessage =  function(msg)
{
    var secs = msg.content.toString().split('.').length - 1;

    console.log(" [x] Received %s ", msg.content.toString(), msg.fields.routingKey);

    setTimeout(function () {
        console.log(" [x] Done");
        channelWrapper.nack(msg,false, false);
    }, secs * 1000);


}