console.log("RabbitMq Receiever Test");

const queueName = 'NodeQueueRabbit';

//const broker = require('./broker');

//const queue = {
//    name: queueName,
//    options: {
//        durable: true
//    }
// };

//const routingKey = 'routing.key.name';

//broker.consume({ queue, routingKey }, (message) => {

//    try {

//        console.log(message);
//    }
//    catch (err) {

//        throw err;


//    }


    
//});


const amqp = require('amqplib/callback_api');


amqp.connect('amqp://localhost', function (error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function (error1, channel) {
        if (error1) {
            throw error1;
        }
        var queue = 'NodeQueue';

        channel.assertQueue(queue, {
            durable: true,
        });

        channel.bindQueue(queue, "Registration", 'Q1');
        //channel.prefetch(1);
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

        channel.consume(queue, function (msg) {

            var secs = msg.content.toString().split('.').length - 1;

            console.log(" [x] Received %s ", msg.content.toString(), msg.fields.routingKey);

            setTimeout(function () {
                console.log(" [x] Done");
                channel.ack(msg);
            }, secs * 1000);
        }, {
            // manual acknowledgment mode,
            // see https://www.rabbitmq.com/confirms.html for details
            noAck: false
        });
    });
});