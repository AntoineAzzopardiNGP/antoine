const amqp = require('amqplib/callback_api');

var amqpConn = null;
start();

function start() {



    amqp.connect('amqp://localhost' + "?heartbeat=60", function (err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(start, 1000);
        }
        conn.on("error", function (err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        conn.on("close", function () {
            console.error("[AMQP] reconnecting");
            return setTimeout(start, 1000);
        });
        console.log("[AMQP] connected");
        amqpConn = conn;
        whenConnected();
    });
}

function whenConnected() {
    startListener()
}

function startListener() {

    amqpConn.createChannel(function (error1, channel) {
        if (error1) {
            throw error1;
        }
        var queue = 'NodeQueue';

        channel.assertQueue(queue, {
            durable: true,
        });

        channel.bindQueue(queue, "Registration", 'Q1');
        //channel.prefetch(1);
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

        channel.consume(queue, function (msg) {

            var secs = msg.content.toString().split('.').length - 1;

            console.log(" [x] Received %s ", msg.content.toString(), msg.fields.routingKey);

            setTimeout(function () {
                console.log(" [x] Done");
                channel.ack(msg);
            }, secs * 1000);
        },
            {
            // manual acknowledgment mode,
            // see https://www.rabbitmq.com/confirms.html for details
            noAck: false
        });
    });




}