
console.log("RabbitMq Server Test");

const host = "amqp://antoine:bMqC3g6n@104.199.5.197:5672"

const amqp = require('amqplib');

let qName = "sample";
let routingKey = "words";
let exchangeName = "grandtour";

var open = amqp.connect(host);

open
    .then(conn => {
        return conn.createChannel();
    })
    .then(ch => {
        return ch
            .assertExchange(exchangeName, "direct", { durable: true })
            .then(() => { return ch.assertQueue(qName, { exclusive: false }); })
            .then(q => { return ch.bindQueue(q.queue, exchangeName, routingKey); });
    })
    .catch(err => {
        console.err(err);
        process.exit(1);
    });

let date = new Date();

addMessage(`This is a node test ${date}`)
    .then(() => {
        StopPressToContinue("Press [Enter] to continue...", false)
        var message = getMessage().then(() => {
            console.log("Processed");
            StopPressToContinue("Press [Enter] to exit...", true);
        });

    });

function StopPressToContinue(message, exit) {
    require('readline')
        .createInterface(process.stdin, process.stdout)
        .question(message, function () {
            if (exit)
                process.exit();

        });

}


async function addMessage(message) {
    return open
        .then(conn => {

            return conn.createChannel();
        })
        .then(ch => {
            ch.publish(exchangeName, routingKey, new Buffer(message));
            let msgTxt = message + " : Message sent at " + new Date();
            console.log(" [+] %s", msgTxt);

            return new Promise(resolve => {
                resolve(message);
            });
        });
}

async function getMessage() {
    return open
        .then(conn => {
            return conn.createChannel();
        })
        .then(ch => {
            return ch.get(qName, {})
                .then(msgOrFalse => {
                    return new Promise(resolve => {
                        let result = "No messages in queue";
                        if (msgOrFalse !== false) {
                            result =
                                msgOrFalse.content.toString() +
                                " : Message received at " +
                                new Date();
                            ch.ack(msgOrFalse);
                        }
                        console.log(" [-] %s", result);
                        resolve(result);
                    });
                });
        });
}

