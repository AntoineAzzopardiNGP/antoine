// broker.js
const Broker = require('roger-rabbit');
const host = "amqp://guest:guest@localhost:5672";
const exchange = "Registration";

module.exports = Broker({
    host: host,
    exchange: {
        type: 'direct',
        name: exchange,
    },
});