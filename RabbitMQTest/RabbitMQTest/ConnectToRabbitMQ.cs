﻿using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;
using System;

namespace RabbitMQTest
{
    public abstract class ConnectToRabbitMQ : IDisposable
    {
        protected IModel Model { get; set; }
        protected IConnection Connection { get; set; }
        public string HostName { get; set; }
        public string Exchange { get; set; }
        public string ExchangeTypeName { get; set; }
        public string QueueName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public ConnectToRabbitMQ(string server, string exchange, string exchangeType, string queueName = "", string userName = "", string password = "")
        {
            HostName = server;
            Exchange = exchange;
            ExchangeTypeName = exchangeType;
            QueueName = queueName;
            UserName = userName;
            Password = password;
        }

        //Create the connection, Model and Exchange(if one is required)
        public virtual bool Connect()
        {
            try
            {
                var connectionFactory = new ConnectionFactory();
                
                connectionFactory.AutomaticRecoveryEnabled = true;
                
                connectionFactory.HostName = HostName;

                if (!String.IsNullOrEmpty(UserName))
                    connectionFactory.UserName = UserName;

                if (!String.IsNullOrEmpty(Password))
                    connectionFactory.Password = Password;

                Connection = connectionFactory.CreateConnection();

                return true;
            }
            catch (BrokerUnreachableException)
            {
                return false;
            }
        }

        public void CreateExchangeQueue(string queueName = "", string routingKey = "")
        {
            try
            {
                Model = Connection.CreateModel();

                bool durable = true;

                if (!String.IsNullOrEmpty(Exchange))
                    Model.ExchangeDeclare(Exchange, ExchangeTypeName, durable);
                else
                    throw new Exception("Exchange Not Defined");

                if (!String.IsNullOrEmpty(queueName))
                {
                    QueueName = queueName;
                    Model.QueueDeclare(queueName, durable, false, false);

                    if (!String.IsNullOrEmpty(routingKey))
                        Model.QueueBind(queueName, Exchange, routingKey);
                }
            }
            catch (Exception)
            {
                 throw;
            }
        }

        public void Dispose()
        {
            if (Connection != null)
                Connection.Close();
            if (Model != null)
                Model.Abort();
        }
    }
}