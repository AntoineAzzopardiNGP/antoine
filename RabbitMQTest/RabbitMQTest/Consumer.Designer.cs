﻿namespace RabbitMQTest
{
    partial class Consumer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.cmbExchangeType = new System.Windows.Forms.ComboBox();
            this.txtQueue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtExchange = new System.Windows.Forms.TextBox();
            this.txtEchange = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRoute = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(28, 140);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(278, 117);
            this.richTextBox1.TabIndex = 6;
            this.richTextBox1.Text = "";
            // 
            // cmbExchangeType
            // 
            this.cmbExchangeType.FormattingEnabled = true;
            this.cmbExchangeType.Location = new System.Drawing.Point(104, 14);
            this.cmbExchangeType.Name = "cmbExchangeType";
            this.cmbExchangeType.Size = new System.Drawing.Size(121, 21);
            this.cmbExchangeType.TabIndex = 1;
            // 
            // txtQueue
            // 
            this.txtQueue.Location = new System.Drawing.Point(104, 66);
            this.txtQueue.Name = "txtQueue";
            this.txtQueue.Size = new System.Drawing.Size(121, 20);
            this.txtQueue.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Type";
            // 
            // txtExchange
            // 
            this.txtExchange.Location = new System.Drawing.Point(104, 41);
            this.txtExchange.Name = "txtExchange";
            this.txtExchange.Size = new System.Drawing.Size(121, 20);
            this.txtExchange.TabIndex = 2;
            // 
            // txtEchange
            // 
            this.txtEchange.AutoSize = true;
            this.txtEchange.Location = new System.Drawing.Point(25, 48);
            this.txtEchange.Name = "txtEchange";
            this.txtEchange.Size = new System.Drawing.Size(55, 13);
            this.txtEchange.TabIndex = 3;
            this.txtEchange.Text = "Exchange";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Queue";
            // 
            // txtRoute
            // 
            this.txtRoute.Location = new System.Drawing.Point(104, 92);
            this.txtRoute.Name = "txtRoute";
            this.txtRoute.Size = new System.Drawing.Size(121, 20);
            this.txtRoute.TabIndex = 4;
            this.txtRoute.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Route";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(231, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Subscribe";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Consumer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 271);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtEchange);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtExchange);
            this.Controls.Add(this.txtRoute);
            this.Controls.Add(this.txtQueue);
            this.Controls.Add(this.cmbExchangeType);
            this.Controls.Add(this.richTextBox1);
            this.Name = "Consumer";
            this.Text = "Consumer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Consumer_FormClosing);
            this.Load += new System.EventHandler(this.Consumer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ComboBox cmbExchangeType;
        private System.Windows.Forms.TextBox txtQueue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtExchange;
        private System.Windows.Forms.Label txtEchange;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRoute;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}