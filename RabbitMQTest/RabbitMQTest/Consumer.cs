﻿using RabbitMQ.Client.Events;
using RabbitMQTest.Class;
using System;
using System.Windows.Forms;


namespace RabbitMQTest
{
    public partial class Consumer : Form
    {


        public string HOST_NAME = "localhost";
        public string EXCHANGE_NAME = "logs";

        private ConsumerService consumerService;

        public Consumer()
        {
            InitializeComponent();
           
        }

        //delegate to post to UI thread
        private delegate void showMessageDelegate(string message);

        //Callback for message receive
        public void handleMessage(BasicDeliverEventArgs e, byte[] message)
        {
            showMessageDelegate s = new showMessageDelegate(richTextBox1.AppendText);
            this.Invoke(s, System.Text.Encoding.UTF8.GetString(message) + $" Routing {e.RoutingKey} " + Environment.NewLine);
        }

        private void Consumer_FormClosing(object sender, FormClosingEventArgs e)
        {
            consumerService.onMessageReceived -= handleMessage;
            consumerService.Dispose();
        }

        private void Consumer_Load(object sender, EventArgs e)
        {
            cmbExchangeType.DataSource = RMQEnums.EnumExchangeType.GetValues(typeof(RMQEnums.EnumExchangeType));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var type = cmbExchangeType.SelectedItem;
            var exchange = txtExchange.Text;
            var queue = txtQueue.Text;
            var routeKey = txtRoute.Text;

            consumerService = new ConsumerService(HOST_NAME, exchange, type.ToString().ToLower());

            if (!consumerService.Connect())
            {
                //Show a basic error if we fail
                MessageBox.Show("Could not connect to Broker");
            }
            else
            {
                consumerService.CreateExchangeQueue(queue, routeKey);
            }

            //Register for message event
            consumerService.onMessageReceived += handleMessage;

            consumerService.onMessageReceived += (et, model) =>
            {
                showMessageDelegate s = new showMessageDelegate(richTextBox1.AppendText);
                this.Invoke(s, System.Text.Encoding.UTF8.GetString(model) + Environment.NewLine);
            };

            //Start consuming
            consumerService.StartConsuming();

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
