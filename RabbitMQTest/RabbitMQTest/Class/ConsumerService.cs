﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQTest.Class
{
    public class ConsumerService : ConnectToRabbitMQ
    {
        protected bool isConsuming;

        // used to pass messages back to UI for processing
        public delegate void onReceiveMessage(BasicDeliverEventArgs e, byte[] message);
        public event onReceiveMessage onMessageReceived;

        public ConsumerService(string server, string exchange, string exchangeType) : base(server, exchange, exchangeType)
        {

        }

        private delegate void ConsumeDelegate();

        public void StartConsuming()
        {
            Model.BasicQos(0, 1, false);

            if (string.IsNullOrEmpty(QueueName))
            {
                QueueName = Model.QueueDeclare();
                Model.QueueBind(QueueName, Exchange, "");
            }

            isConsuming = true;

            ConsumeDelegate c = new ConsumeDelegate(Consume);
            c.BeginInvoke(null, null);
        }

        protected Subscription mSubscription { get; set; }

        private void Consume()
        {
            bool autoAck = false;

            //create a subscription
            mSubscription = new Subscription(Model, QueueName, autoAck);

            while (isConsuming)
            {
                BasicDeliverEventArgs e = mSubscription.Next();
                byte[] body = e.Body;
                onMessageReceived(e, body);
                mSubscription.Ack(e);

            }
        }

        public void Dispose()
        {
            isConsuming = false;
            base.Dispose();
        }
    }
}
