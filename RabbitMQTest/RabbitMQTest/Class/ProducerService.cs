﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQTest.Class
{
    public class ProducerService : ConnectToRabbitMQ
    {
        public ProducerService(string server, string exchange, string exchangeType) : base(server, exchange, exchangeType)
        {
        }

        public void SendMessage(byte[] message, string routingKey = "")
        {
            IBasicProperties basicProperties = Model.CreateBasicProperties();
            basicProperties.Persistent= true;
            Model.BasicPublish(Exchange, routingKey , basicProperties, message);
        }

    }
}
