﻿using System;
using System.Windows.Forms;
using static RabbitMQTest.RMQEnums;

namespace RabbitMQTest
{
    public partial class Main : Form
    {

        public string HOST_NAME = "localhost";
        
        public string EXCHANGE_NAME = "logs";

        private Class.ProducerService producerService;

        public Main()
        {
            InitializeComponent();
        }

        private void OpenConnection(EnumExchangeType type, string exchangeName= "")
        {
            producerService = new Class.ProducerService(HOST_NAME, string.IsNullOrEmpty(exchangeName) ?EXCHANGE_NAME: exchangeName, type.ToString().ToLower());

            if (!producerService.Connect())
            {
                //Show a basic error if we fail
                MessageBox.Show("Could not connect to Broker");
            }
            else
            {
                producerService.CreateExchangeQueue(txtQueueName.Text, txtRoutingKey.Text);
            }

        }



        private int count = 0;

        private void btnSend_Click(object sender, EventArgs e)
        {
            string message = String.Format("{0} - {1}", count++, textBox1.Text);
            producerService.SendMessage(System.Text.Encoding.UTF8.GetBytes(message), txtRoutingKey.Text);
        }

        private void BtnAddConsumer_Click(object sender, EventArgs e)
        {
            //Open a new Consumer Form
            Consumer consumer = new Consumer();
            consumer.Show();

        }
        
        private void Producer_Load(object sender, EventArgs e)
        {

            cmbExchangeType.DataSource = RMQEnums.EnumExchangeType.GetValues(typeof(RMQEnums.EnumExchangeType));
            cmbExchangeType.SelectedItem = EnumExchangeType.Fanout;
                       
        }

        private void cmbExchangeType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            OpenConnection((EnumExchangeType)cmbExchangeType.SelectedItem ,txtExchangeName.Text);
        }

       
        private void BtnAddProduver_Click(object sender, EventArgs e)
        {
            Producer producer = new Producer();
            producer.Show();
        }
    }
}
