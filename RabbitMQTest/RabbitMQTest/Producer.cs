﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static RabbitMQTest.RMQEnums;

namespace RabbitMQTest
{
    public partial class Producer : Form
    {

        public string HOST_NAME = "localhost";

        public string EXCHANGE_NAME = "logs";

        private Class.ProducerService producerService;

        public Producer()
        {
            InitializeComponent();
        }

        private void OpenConnection(EnumExchangeType type, string exchangeName = "")
        {
            producerService = new Class.ProducerService(HOST_NAME, string.IsNullOrEmpty(exchangeName) ? EXCHANGE_NAME : exchangeName, type.ToString().ToLower());

            if (!producerService.Connect())
            {
                //Show a basic error if we fail
                MessageBox.Show("Could not connect to Broker");
            }
            else
            {
                producerService.CreateExchangeQueue(txtQueueName.Text, txtRoutingKey.Text);
            }

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenConnection((EnumExchangeType)cmbExchangeType.SelectedItem, txtExchangeName.Text);
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (producerService != null)
                producerService.CreateExchangeQueue(txtQueueName.Text, txtRoutingKey.Text);
            else
                MessageBox.Show("No connection", "Error");
        }

        private int count = 0;

        private void btnSend_Click(object sender, EventArgs e)
        {
            string message = rtbMessage.Text;
            producerService.SendMessage(System.Text.Encoding.UTF8.GetBytes(message), txtMessageRouteKey.Text);
        }

        private void Producer_Load(object sender, EventArgs e)
        {
            cmbExchangeType.DataSource = RMQEnums.EnumExchangeType.GetValues(typeof(RMQEnums.EnumExchangeType));
        }
    }
}
