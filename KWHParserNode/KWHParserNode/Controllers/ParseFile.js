

class ParserTest { 

   


    async ParseFile({ request, response }) {

        const fs = require('fs');
        const es = require('event-stream');
       
        var filename = 'C:\TestFiles\test1.txt';

        var vertexes_number, edges_number;
        var edges = [];

        fs.createReadStream(filename)
            .pipe(es.split()) // split by lines
            .pipe(es.map(function (line, next) {
                // split and convert all to numbers
                edges.push(line.split(' ').map((n) => +n));

                next(null, line);
            })).pipe(es.wait(function (err, body) {
                // the first element is an array containing vertexes_number
                vertexes_number = edges.shift().pop();

                // the following element is an array containing edges_number
                edges_number = edges.shift().pop();

                console.log('done');
                console.log('vertexes_number: ' + vertexes_number);
                console.log('edges_number: ' + edges_number);
                console.log('edges: ' + JSON.stringify(edges, null, 3));
            }));

    }

    module.exports = ParserTest
}