const { v4: uuidv4 }    = require('uuid');
const rascal            = require('rascal');
const Logger            = use('Logger');
const definitions       = use('App/Services/Rascal/Config/JSON/config.json');
const Broker            = rascal.BrokerAsPromised;
const config            = rascal.withDefaultConfig(definitions)

const publicationName   = 'app_publication';
const subscriptionName  = 'app_subscription';
const deadKeyRouteKey   = 'npg.deadletter.key';

let broker              = null;

class RMQRascalProvider {


/** @description Starts RabbitMq instancse as specified in the config.json file
*   @return {broker}
*/
  static async start() {
    try {
      if (!broker) {
        broker = await Broker.create(config);
        broker.on('error', Logger.error);
       
      }
      return broker;
    }
    catch (err) {

      Logger.error(err);
    } 
  }

/** @description Starts RabbitMq message consumer as specified in the config.json file.
*   @param {broker} broker The broker returned form the start method.
*   @param {callback} callback The callback that will process the messages.
*   @return {broker}
*/
  static async startReceivingMessages(broker, callback) {
    try {
      const subscription = await broker.subscribe(subscriptionName);
      subscription.on('message', (message, content, ackOrNack) => {

        callback(message);
        Logger.info(content);
        Logger.info(message);
        ackOrNack();

      }).on('error', function (err, message, ackOrNack) {

        Logger.warning(err);
        Logger.warning(message);
        ackOrNack("Error", [{ strategy: 'forward', publication: publicationName, options: { routingKey: deadKeyRouteKey } }, { strategy: 'ack' }])
      });
    }
    catch (err) {
      Logger.warning(err);
    }
    finally {
      return broker;
    }
  }

/** @description Starts RabbitMq message consumer as specified in the config.json file.
*   @param {broker} broker The broker returned form the start method.
*   @param {message} message The message to be sent to the exchange that will process the messages.
*   @return {broker}
*/

  static async publishMessage(broker, message) {

    try {
      let messageOptions = { options: { correlationId: uuidv4(), contentType: "text/plain" } }
      let publication = await broker.publish( publicationName , message, messageOptions);

      publication.on('error', function (err) { Logger.error(err) });

    }
    catch (err) {
      Logger.warning(err);
    }
    finally {
      return broker;
    }

  }

}

module.exports = RMQRascalProvider
