'use strict'

const publish = use('App/Services/Rascal/RMQRascalProvider');

class RabbitMqController {

  async sendMessage() {

    //publish.start().then(broker => publish.publishMessage(broker, 'Hello From controller Class - ' + new Date()));

    let broker = await publish.start();
    publish.publishMessage(broker, 'Hello From controller Class - ' + new Date());

    return 'Dummy response';

  }

}

module.exports = RabbitMqController
