module.exports = {
  "rascal": {
    "vhosts": {
      "v1": {
        "assert":true,
        "namespace": false,
        "connection": {
          "url": "amqp://guest:guest@localhost/"
        }
        ,
        "exchanges": {
          "demo_ex": {
            "type": "direct",
            "options": { "durable": true }
          },
          "dead_letter": {
            "type": "direct",
            "options": { "durable": true }
          }
        },
        "queues": {
          "demo_q": { "options": { "durable": true } },
          "demo_u": {},
          "demo_dead": {}
        },
        "bindings": {
          "demo_ex[a.b.c] -> demo_q": {},
        
        },
        "publications": {
          "demo_pub": {
            "exchange": "demo_ex",
            "routingKey": "a.b.c"
          }
        },
        "subscriptions": {
          "demo_sub": {
            "queue": "demo_q",
            "prefetch": 3,
            "options": {
              "arguments": {
                "x-dead-letter-exchange": "dead_letter",
                "x-dead-letter-routing-key": "demo_dead.key"
              }
            }
          }
        }
      }
    }

  }
}
