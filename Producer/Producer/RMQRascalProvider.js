const rascal = require('rascal');
const { v4: uuidv4 } = require('uuid');
const definitions = require('./Rascal/Config/config.json');
const config = rascal.withTestConfig(definitions)
const Broker = rascal.BrokerAsPromised;

class RMQRascalProvider {


  static async start() {
    try {

      let broker = await Broker.create(config);
      broker.on('error', console.error);
      return broker;
    }
    catch (err) {
      console.error(err);
    }
  }

  static async startReceiveMessage(broker) {
   try {

      const subscription = await broker.subscribe('demo_sub');
      subscription.on('message', (message, content, ackOrNack) => {
        console.log(content);
        console.log(message);
        ackOrNack();
      }).on('error', function (err, message, ackOrNack) {
        ackOrNack("Error", [{ strategy: 'forward', publication: 'demo_pub', options: { routingKey: 'demo_dead.key' } }, { strategy: 'ack' }])
      });
    }
    catch (err) {
      console.error;
    }
    finally {
      return broker;
    }
  }

  static async publishMessage(broker, message) {

    try {
      let messageOptions = { options: { correlationId: uuidv4(), contentType: "text/plain" } }

      // Publish a message
      let publication = await broker.publish('demo_pub', message, messageOptions);
      publication.on('error', console.error);
    }
    catch (err) {

      console.error;
    }
    finally {
      return broker;
    }

  }





}

module.exports = RMQRascalProvider
