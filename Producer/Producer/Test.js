
try {
  const test = require('./RMQRascalProvider');

  test.start().then(broker => test.publishMessage(broker,'Hello From Test Class'));
  test.start().then(broker => test.startReceiveMessage(broker));

  //test.publishMessage(broker, "On this is new");
  //test.startReceiveMessage(broker);
  console.log("OK");

}
catch (err) {

  console.error(err);

}
