const rascal = require('rascal');
const { v4: uuidv4 } = require('uuid');

const config = require('./rascal/config/config')

const definitions = require('./Rascal/Config/config.json');
//const config = rascal.withTestConfig(definitions)
const Broker = rascal.BrokerAsPromised;

(async () => {
  try {

    const broker = await Broker.create(config);
    broker.on('error', console.error);

    let messageOptions = { options: { messageId: 'wibble', correlationId: uuidv4(), contentType: "text/plain" } }

    // Publish a message
    const publication = await broker.publish('demo_pub', 'Hello World!', messageOptions);
    publication.on('error', console.error);

    // Consume a message
    const subscription = await broker.subscribe('demo_sub');
    subscription.on('message', (message, content, ackOrNack) => {

      console.log(content);

      console.log(message);


      ackOrNack("Error", [{ strategy: 'forward', publication: 'demo_pub', options: { routingKey: 'demo_dead.key' } }, { strategy: 'ack' }])
      //ackOrNack();
    }).on('error', function (err, message, ackOrNack) {
      console.error;

    });
  }
  catch (err) {
    console.error(err);
  }
})();
