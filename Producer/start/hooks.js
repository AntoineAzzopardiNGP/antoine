const { hooks } = require('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
  const View = use('View')
  View.global('time', () => new Date().getTime())

  const listener = use('App/Services/Rascal/RMQRascalProvider');
  listener.start().then(broker => listener.startReceivingMessages(broker, useMessage));

  function useMessage(message) {
     console.log(message)
  }

})
