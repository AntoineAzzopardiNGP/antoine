﻿using System;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace KWHParser
{
    class Program
    {

        private static readonly string file = @"c:\testfiles\test1.txt";

        static void Main()
        {
            //WriteBigFile();

            SkipLines(file);

            return;


            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();

            var results = ReadFile(file);

            stopWatch.Stop();

            // Get the elapsed time as a TimeSpan value.
            int tsParser = stopWatch.Elapsed.Milliseconds;

            stopWatch.Restart();

            var res = results.Sum();

            stopWatch.Stop();

            int tsSum = stopWatch.Elapsed.Milliseconds;

            // Format and display the TimeSpan value.
            //string elapsedTimeParser = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", tsParser.Hours, tsParser.Minutes, tsParser.Seconds, tsParser.TotalMilliseconds / 10);
            //string elapsedTimeSum = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", tsSum.Hours, tsSum.Minutes, tsSum.Seconds, tsSum.Milliseconds / 10);

            Console.WriteLine($"Parse Time Parse:{tsParser}ms - Sum Time Parse:{tsSum}ms - Answer:{res}");
        }


        private static void WriteBigFile()
        {
            string path = @"c:\testfiles\BigFile.txt";
            
            var numbers = ReadFileString(file);

            // This text is added only once to the file.
            if (!File.Exists(path))
            {
                // Create a file to write to.
                
                using (StreamWriter sw = File.CreateText(path))
                {
                    WriteToFile(sw, numbers);
                }
            }

        }
        

        private static decimal[] ReadFile(string textFile)
        {
            List<decimal> kwh = new List<decimal>();

            using (StreamReader file = new StreamReader(textFile))
            {
                
                string ln;
                
                while ((ln = file.ReadLine()) != null)
                {
                    kwh.Add(Decimal.Parse(ln));
                }
                file.Close();

            }

            return kwh.ToArray();
        }

        private static string[] ReadFileString(string textFile)
        {
            List<string> kwh = new List<string>();

            using (StreamReader file = new StreamReader(textFile))
            {

                string ln;

                while ((ln = file.ReadLine()) != null)
                {
                    kwh.Add(ln);
                }
                file.Close();

            }

            return kwh.ToArray();
        }
        
        private static void WriteToFile(StreamWriter sw, string[] numbers)
        {
            Random rnd = new Random();

            int yearsecon = 365 * 24 * 60;

            int i = 0;
            while (i < yearsecon)
            {
                sw.WriteLine(numbers[rnd.Next(0, numbers.Length - 1)]);
                i++;
            }


        }

        private static void SkipLines(string textFile)
        {

            var lines = File.ReadLines(textFile);
            var i = 0;
            foreach(string line in lines.Skip(10))
            {
                Console.WriteLine(line);
                i++;

                if (i > 100)
                    break;
            }
        
        
        
        }
    }
}
